#include <QCoreApplication>
#include <QCommandLineParser>
#include <QDir>

#include <functional>

#include <QTextStream>

#include "taskmanager.h"


QTextStream _stdout(stdout);

void fillCommandLineOptions(QCommandLineParser &parser);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;
    fillCommandLineOptions(parser);

    parser.process(a);
    if (parser.positionalArguments().count() == 0)
    {
        _stdout << parser.helpText() << Qt::flush;
        return 0;
    }

    QString dbName;
    if (!parser.isSet("database"))
    {
        _stdout << "Database not specified" << Qt::endl;
        return -1;
    }
    else
        dbName = parser.value("database");

    QString tableName;
    if (!parser.isSet("table"))
    {
        _stdout << "Table not specified" << Qt::endl;
        return -1;
    }
    else
        tableName = parser.value("table");

    QCryptographicHash::Algorithm method = QCryptographicHash::Sha1;
    QString strAlgorithm =
            parser.isSet("algorithm") ?
                parser.value("algorithm").toLower() :
                QStringLiteral("sha1");

    if (strAlgorithm == QStringLiteral("md5"))
        method = QCryptographicHash::Md5;
    else if (strAlgorithm == QStringLiteral("sha1"))
        method = QCryptographicHash::Sha1;
    else if (strAlgorithm == QStringLiteral("sha256"))
        method = QCryptographicHash::Sha256;
    else if (strAlgorithm == QStringLiteral("sha512"))
        method = QCryptographicHash::Sha512;
    else
    {
        strAlgorithm = QStringLiteral("sha1");
        method = QCryptographicHash::Sha1;
    }

    QStringList masks;
    if (parser.isSet("masks"))
        masks = parser.value("masks").split(";", Qt::SkipEmptyParts);
    else
        masks.append("*");

    const QString path(parser.positionalArguments().first());

    _stdout << "Database: " << dbName <<
               "\nTable: " << tableName <<
               "\nClear: " << (parser.isSet("clear") ? "true" : "false") <<
               "\nAlgorithm: " << strAlgorithm <<
               "\nDirectory: " << path <<
               "\nMasks: " << masks.join(", ") << Qt::endl << Qt::endl;


    TaskManager manager(dbName, tableName, parser.isSet("clear"), method, &a);
    QObject::connect(&manager, &TaskManager::sigDone, &a, &QCoreApplication::quit);



    if (!manager.start(path, masks))
        return -1;

    return a.exec();


}

void fillCommandLineOptions(QCommandLineParser &parser)
{
    parser.addOption(QCommandLineOption(
                         QStringList() << "d" << "database", "Database file.", "database"));

    parser.addOption(QCommandLineOption(
                         QStringList() << "t" << "table", "Table in database.", "table"));

    parser.addOption(QCommandLineOption(
                         QStringList() << "m" << "masks", "File masks separated by \";\".", "masks"));

    parser.addOption(QCommandLineOption(
                         QStringList() << "c" << "clear", "Clear table if exists."));

    parser.addOption(QCommandLineOption(
                         QStringList() <<  "a" << "algorithm", "Algorithm  (md5, sha1, sha256 or sha512).", "algorithm"));

    parser.addPositionalArgument("path", "dir path");


    parser.addHelpOption();
}

