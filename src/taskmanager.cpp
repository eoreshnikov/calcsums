#include "taskmanager.h"

#include "calctask.h"
#include <QThreadPool>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

TaskManager::TaskManager(const QString& dbName,
                         const QString& tableName,
                         bool clearTable, QCryptographicHash::Algorithm method,
                         QObject *parent) :
      QObject(parent),
      _dbName(dbName),
      _tableName(tableName),
      _clearTable(clearTable),
      _method(method),
      _totalCount(0),
      _processedCount(0),
      _stdOut(stdout),
      _stdErr(stderr)

{

}

bool TaskManager::start(const QString& path, const QStringList& nameFilters)
{
    if (!initDatabase())
        return false;

    processingDir(path, nameFilters);

    if (_taskList.isEmpty())
    {
        _stdOut << "No files found." << Qt::endl;
        return false;
    }

    return true;
}

void TaskManager::slDataReady(const TaskResult& result)
{
    const QString strQuery(
                QStringLiteral("INSERT INTO %1 VALUES(:file, :hash)")
                .arg(_tableName));

    QSqlDatabase::database().transaction();
    QSqlQuery query;
    for (const QPair<QString, QString> &pair : result)
    {
        query.prepare(strQuery);
        query.bindValue(QStringLiteral(":file"),  pair.first);
        query.bindValue(QStringLiteral(":hash"),  pair.second);
        if (!query.exec())
            _stdErr << query.lastError().text() << Qt::endl;
    }
    QSqlDatabase::database().commit();
    _processedCount += result.count();

    int precent = _totalCount != 0 ?
                (int)(_processedCount * 100.0 / _totalCount) : 0;

    _stdOut << QStringLiteral("\r                              \r")
            << QStringLiteral("%1 / %2 [%3%]")
               .arg(_processedCount)
               .arg(_totalCount)
               .arg(precent)
            << Qt::flush;

}

void TaskManager::slTaskEnd()
{
    CalcTask* task = qobject_cast<CalcTask*>(sender());
    Q_ASSERT_X(task != nullptr, Q_FUNC_INFO, "task is nullptr");

    _taskList.removeAll(task);
    delete task;

    if (_taskList.isEmpty())
    {
        _stdOut << "\nFinished.             " << Qt::endl;
        emit sigDone();
    }
}

void TaskManager::processingDir(const QDir& dir, const QStringList &nameFilters)
{
    std::function <void(const QStringList&)> makeTask;
    makeTask = [this](const QStringList &filesList)
    {
        CalcTask* task = new CalcTask(filesList, _method);
        task->setAutoDelete(false);
        connect(task, &CalcTask::sigDataReady, this, &TaskManager::slDataReady, Qt::QueuedConnection);
        connect(task, &CalcTask::sigDone, this, &TaskManager::slTaskEnd, Qt::QueuedConnection);
        _taskList.append(task);
        _totalCount += filesList.count();
        QThreadPool::globalInstance()->start(task, QThread::LowPriority);

    };

    const int listPartSize = 100;

    QList<QDir> dirs({dir});

    std::function <void(const QDir&)> makeDirsList;
    makeDirsList = [&dirs, &makeDirsList](const QDir &d)
    {
        const QFileInfoList &items =
                d.entryInfoList({"*"}, QDir::Dirs | QDir::NoDotAndDotDot, QDir::DirsLast);
        for (QFileInfo fi : items)
        {
            if (!fi.isDir())
                continue;
            dirs.append(fi.filePath());
            makeDirsList(fi.filePath());
        }
    };

    makeDirsList(dir);

    for (const QDir &d : dirs)
    {
        QStringList fileList;
        const QFileInfoList &items =
                d.entryInfoList(nameFilters, QDir::Files | QDir::NoDotAndDotDot, QDir::DirsLast);
        for (QFileInfo fi : items)
        {
            if (fi.isDir())
                continue;

            fileList.append(fi.filePath());

            if (fileList.count() >= listPartSize)
            {
                makeTask(fileList);
                fileList.clear();
            }
        }
        if (!fileList.isEmpty())
            makeTask(fileList);
    }
}

bool TaskManager::initDatabase()
{
    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(_dbName);
    if (!database.open())
    {
        _stdErr << database.lastError().text() << Qt::flush;
        return false;
    }

    if (database.tables().contains(_tableName))
    {
        if (_clearTable)
        {
            QSqlQuery query;
            const QString strQuery(
                        QStringLiteral("DELETE FROM %1")
                        .arg(_tableName));
            if (!query.exec(strQuery))
            {
                _stdErr << database.lastError().text() << Qt::flush;
                return false;
            }
        }
    }
    else
    {
        QSqlQuery query;
        const QString strQuery(
                    QStringLiteral("CREATE TABLE %1(file TEXT, hash TEXT);")
                    .arg(_tableName));
        if (!query.exec(strQuery))
        {
            _stdErr << database.lastError().text() << Qt::flush;
            return false;
        }
    }

    return true;
}
