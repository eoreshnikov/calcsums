#ifndef CALCTASK_H
#define CALCTASK_H

#include <QObject>
#include <QRunnable>
#include <QString>
#include <QStringList>
#include <QHash>
#include <QCryptographicHash>

typedef QList <QPair<QString, QString>> TaskResult;

class CalcTask : public QObject, public QRunnable
{
    Q_OBJECT
public:
    CalcTask(const QStringList &filelist, QCryptographicHash::Algorithm method);
    void run();

signals:
    void sigDataReady(const TaskResult data);
    void sigDone();

private:
    QStringList _filesList;
    QCryptographicHash::Algorithm _method;

    static const int RESULT_MAX_COUNT;

};

#endif // CALCTASK_H
