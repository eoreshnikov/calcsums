#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include <QObject>
#include <QDir>
#include <QList>
#include <QTimer>
#include <QSqlDatabase>
#include <QTextStream>

#include "calctask.h"


class TaskManager : public QObject
{
    Q_OBJECT
public:
    explicit TaskManager(const QString &dbName,
                         const QString &tableName,
                         bool clearTable,
                         QCryptographicHash::Algorithm method,
                         QObject *parent = nullptr);


    bool start(const QString &path, const QStringList &nameFilters);

public slots:
    void slDataReady(const TaskResult &result);
    void slTaskEnd();

signals:
    void sigDone();


private:
    void processingDir(const QDir &dir, const QStringList& nameFilters);
    bool initDatabase();

    QList<CalcTask*> _taskList;
    QString _dbName;
    QString _tableName;
    bool _clearTable;
    QCryptographicHash::Algorithm _method;

    quint32 _totalCount;
    quint32 _processedCount;

    QTextStream _stdOut;
    QTextStream _stdErr;
};

#endif // TASKMANAGER_H
