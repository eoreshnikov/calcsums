#include "calctask.h"

#include <QFile>
#include <QCryptographicHash>

const int CalcTask::RESULT_MAX_COUNT = 30;

CalcTask::CalcTask(const QStringList& fileslist, QCryptographicHash::Algorithm method) :
    _filesList(fileslist),
    _method(method)
{

}

void CalcTask::run()
{
    TaskResult result;

    for (const QString &fileName : _filesList)
    {
        QFile file(fileName);
        file.open(QIODevice::ReadOnly);

        QCryptographicHash hash(_method);
        if (!hash.addData(&file))
            continue;

        file.close();

        result.append(qMakePair(fileName, hash.result().toBase64()));

        if (result.count() >= RESULT_MAX_COUNT)
        {
            emit sigDataReady(result);
            result.clear();
        }
    }
    if (!result.isEmpty())
        emit sigDataReady(result);

    emit sigDone();
}
