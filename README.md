#### Поиск файлов в директории и запись их контрольных сумм в базу данных

##### Require  Qt5 (>= 5.15.8) and driver sqlite3 for Qt5.

### Simple build:
```
qmake CONFIG+=release
make
```

### Build deb package:
```
cd ./mkdeb
fakeroot ./mkdeb.sh ../
```

### Usage

##### Params:

**-d --database <database>** Database file. **Required parameter**.

**-t --table <table>** Table in database. **Required parameter**.

**-m --masks <masks>** File masks separated by ";". Default value: "*".

**-c --clear**  Clear table if exists

**-a --algorithm <algorithm>** Algorithm. Values: md5, sha1, sha256 or sha512. Default value "sha1"


##### example:
```
./calcsums -d db.sqlite -t hashes <dir_path>
```
