QT -= gui
QT += sql

CONFIG += c++14 console
CONFIG -= app_bundle
TARGET = calcsums
DESTDIR = bin


DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
        src/calctask.cpp \
        src/main.cpp \
        src/taskmanager.cpp

OBJECTS_DIR = tmp
MOC_DIR     = tmp
RCC_DIR     = tmp
UI_DIR      = tmp

HEADERS += \
    src/calctask.h \
    src/taskmanager.h

