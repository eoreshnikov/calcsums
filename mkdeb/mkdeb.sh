#!/bin/sh

if [ $# -eq 0 ] ; then 
   echo "mkdeb.sh <source_dir>"
   exit 
fi 

SOURCE_DIR=`readlink -f ${1}`

CURENTDIR=`pwd`


cd $SOURCE_DIR
rm ./bin/calcsums

qmake CONFIG+=release
make clean
make
cd $CURENTDIR


DIST_ROOT=distroot
DEST_BIN_DIR=usr/bin

DIST_VERSION=1.0.1-2

mkdir $DIST_ROOT
mkdir $DIST_ROOT/DEBIAN
mkdir -p $DIST_ROOT/$DEST_BIN_DIR

FULL_REVISION=`git log --pretty=format:'%H' -n 1`

cp -P $SOURCE_DIR/bin/calcsums $DIST_ROOT/$DEST_BIN_DIR
chmod 755 $DIST_ROOT/$DEST_BIN_DIR/calcsums

#Создаем контрольный файл
cat debctl_calcsums.template |sed -e "s,%VERSION%,${DIST_VERSION},g" | sed "s/%ARCH%/amd64/g" | sed "s/%REVISION%/${FULL_REVISION}/g"> $DIST_ROOT/DEBIAN/control


find $DIST_ROOT/$DEST_BIN_DIR -type f -name "*" -exec chmod 0755 {} \;

#Сделаем strip и посчитаем контрольные суммы
CURDIR=`pwd`
cd $DIST_ROOT
find $DEST_BIN_DIR -type f -exec strip --strip-unneeded {} \;

find usr -type f -exec md5sum {} \; >> ./DEBIAN/md5sums

cd $CURDIR


chmod 0644 $DIST_ROOT/DEBIAN/*


dpkg-deb --build $DIST_ROOT  calcsums-${DIST_VERSION}_amd64.deb
rm -r $DIST_ROOT
